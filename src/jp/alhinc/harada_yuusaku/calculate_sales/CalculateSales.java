package jp.alhinc.harada_yuusaku.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}


		ArrayList<String> branchNum = new ArrayList<String>();//支店番号
		Map<String,String> branchNameMap = new HashMap<String,String>();//支店名マップ
		Map<String, Long> branchSalesMap = new HashMap<String,Long>();//売り上げ金額マップ



		BufferedReader br = null;
		try{
			File file = new File(args[0], "branch.lst");
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {//readLineは行で分ける
				String str = line;
				String[] branch = str.split(",",0);//文字列を,で分ける
				if(branch.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				branchNum.add(branch[0]);
				branchNameMap.put(branch[0],branch[1]);
				branchSalesMap.put(branch[0],0L);
				if(!branch[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}

		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally{
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}

		}

		File file = new File(args[0]);
		File[] list = file.listFiles();
		ArrayList<String> array = new ArrayList<String>();

		for(int i = 0; i< list.length; i++) {
			String sales =list[i].getName();
			if(sales.matches("^\\d{8}.rcd$") && list[i].isFile()) {
				array.add(sales);
			}
		}
		for(int i = 0; i < array.size()-1; i++ ) {
			String fileName = array.get(i).substring(0,8);
			String fileName2 =array.get(i+1).substring(0,8);
			int fileNa = Integer.parseInt(fileName);
			int fileNa2 = Integer.parseInt(fileName2);
			if(fileNa2 - fileNa != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}




		try{
			for(int i = 0; i< array.size(); i++) {
				String str  =array.get(i);
				File fileArray = new File(args[0], str);
				FileReader fr = new FileReader(fileArray);
				br = new BufferedReader(fr);

				String line;
				ArrayList<String> arrayPair = new ArrayList<String>();
				while((line = br.readLine()) != null) {
					arrayPair.add(line);

				}
				if(!branchNameMap.containsKey(arrayPair.get(0))) {
					System.out.println(array.get(i) + "の支店コードが不正です");
					return;
				}
				if(arrayPair.size() != 2) {
					System.out.println(array.get(i)+"のフォーマットが不正です");
					return;
				}
				if(!arrayPair.get(1).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long ln = Long.parseLong(arrayPair.get(1));

				if(branchSalesMap.get(arrayPair.get(0)) + ln >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}else{
					branchSalesMap.put(arrayPair.get(0),branchSalesMap.get(arrayPair.get(0)) + ln );
				}
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally{
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}

		}
		if(!output(args[0], "branch.out", branchNameMap, branchSalesMap)) {
			return;
		}


	}
	public static boolean output(String args,String fileName, Map<String,String>branchNameMap, Map<String,Long>mapSales) {
		BufferedWriter bw = null;
		try{
			File outfile = new File(args,fileName);
			FileWriter fw = new FileWriter(outfile);
			 bw = new BufferedWriter(fw);
			for(Map.Entry<String,String> entry :branchNameMap.entrySet() ) {

				bw.write(entry.getKey() + "," + entry.getValue() + "," + mapSales.get(entry.getKey()));
				bw.newLine();
			}


		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				System.out.println("予期せぬエラーが発生しました");
				return false;
				}
			}
		}
		return true;
	}
}


